﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Room_Booking
{
    
    public partial class Room_Booking : Form
    {
      
        Room_Bookings_Invites bookings = new Room_Bookings_Invites();

        public Room_Booking()
        {
            InitializeComponent();

            // Define the minumum booking date to todays date
            DateTime minBookingDate = DateTime.Today;
            // Define max booking date to 3 months from today
            DateTime maxBookingDate = minBookingDate.AddMonths(3);

            // Set the min and max date to calender to limit selection
            // to the present
            monthCalendar1.MinDate = minBookingDate;
            monthCalendar1.MaxDate = maxBookingDate;

        }

        #region Buttons
        // Next Button
        private void btn_Two_Click(object sender, EventArgs e)
        {

        Confirm_Booking confirm = new Confirm_Booking(); // makes a local variable for the Confirm_Booking function


            if (rad_Yes.Checked == true)
            {
                Visible = false;
                var result = bookings.ShowDialog(); // shows the dialog box for inviting colleagues if the radio button is checked
                if (result == DialogResult.Abort)
                {
                    Close();
                }
                else
                Visible = true;
            }
            else if (rad_No.Checked == true)
            {
                Visible = false;
                var result = confirm.ShowDialog(); // shows the dialog box for confirming the booking if the radio button for 'no' is checked
                if (result == DialogResult.Abort)
                {
                    Close();
                }
                else
                Visible = true;
            }
        }

        //Exit Button
        private void btn_One_Click(object sender, EventArgs e)
        {
            Close(); // Exits Program
        }
        #endregion

        private void txt_Two_TextChanged(object sender, EventArgs e)
        {

        }
    }
}