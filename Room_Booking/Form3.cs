﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Room_Booking
{
    public partial class Confirm_Booking : Form
    {

        public Confirm_Booking()
        {
            InitializeComponent();
        }

        private void Confirm_Booking_Load(object sender, EventArgs e)
        {

        }

        #region Buttons

        // Exit Button
        private void btn_Exit_Click(object sender, EventArgs e)
        {
            
            DialogResult = DialogResult.Abort;
            Close(); //closes the confirm form and returns to the bookings form
            
        }
        // Back Button
        private void btn_Back_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close(); //closes the confirm button and returns to the prieveous form that was used

        }
        #endregion
    }
}
