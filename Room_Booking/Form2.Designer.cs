﻿namespace Room_Booking
{
    partial class Room_Bookings_Invites
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Ten = new System.Windows.Forms.Label();
            this.lbl_Eleven = new System.Windows.Forms.Label();
            this.lst_Empl = new System.Windows.Forms.ListBox();
            this.btn_Next = new System.Windows.Forms.Button();
            this.btn_Exit = new System.Windows.Forms.Button();
            this.btn_Back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_Ten
            // 
            this.lbl_Ten.AutoSize = true;
            this.lbl_Ten.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Ten.Location = new System.Drawing.Point(137, 18);
            this.lbl_Ten.Name = "lbl_Ten";
            this.lbl_Ten.Size = new System.Drawing.Size(176, 29);
            this.lbl_Ten.TabIndex = 0;
            this.lbl_Ten.Text = "Booking Invites";
            // 
            // lbl_Eleven
            // 
            this.lbl_Eleven.AutoSize = true;
            this.lbl_Eleven.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Eleven.Location = new System.Drawing.Point(121, 59);
            this.lbl_Eleven.Name = "lbl_Eleven";
            this.lbl_Eleven.Size = new System.Drawing.Size(192, 20);
            this.lbl_Eleven.TabIndex = 4;
            this.lbl_Eleven.Text = " Informing other colleages";
            // 
            // lst_Empl
            // 
            this.lst_Empl.FormattingEnabled = true;
            this.lst_Empl.Items.AddRange(new object[] {
            "**EXAMPLE LIST**",
            "Employee 13654631",
            "Employee 15645248",
            "Employee 16791234",
            "Employee 27491683",
            "Employee 31584649",
            "Employee 35465149",
            "Employee 36498132",
            "Employee 69746165",
            "Employee 84983244",
            "Employee 87613454"});
            this.lst_Empl.Location = new System.Drawing.Point(92, 95);
            this.lst_Empl.Name = "lst_Empl";
            this.lst_Empl.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lst_Empl.Size = new System.Drawing.Size(256, 147);
            this.lst_Empl.Sorted = true;
            this.lst_Empl.TabIndex = 5;
            // 
            // btn_Next
            // 
            this.btn_Next.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_Next.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Next.Location = new System.Drawing.Point(276, 284);
            this.btn_Next.Name = "btn_Next";
            this.btn_Next.Size = new System.Drawing.Size(81, 32);
            this.btn_Next.TabIndex = 6;
            this.btn_Next.Text = "Next";
            this.btn_Next.UseVisualStyleBackColor = false;
            this.btn_Next.Click += new System.EventHandler(this.btn_Next_Click);
            // 
            // btn_Exit
            // 
            this.btn_Exit.BackColor = System.Drawing.Color.SandyBrown;
            this.btn_Exit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Exit.Location = new System.Drawing.Point(363, 284);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.Size = new System.Drawing.Size(81, 32);
            this.btn_Exit.TabIndex = 7;
            this.btn_Exit.Text = "Exit";
            this.btn_Exit.UseVisualStyleBackColor = false;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // btn_Back
            // 
            this.btn_Back.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.btn_Back.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btn_Back.Location = new System.Drawing.Point(189, 284);
            this.btn_Back.Name = "btn_Back";
            this.btn_Back.Size = new System.Drawing.Size(81, 32);
            this.btn_Back.TabIndex = 8;
            this.btn_Back.Text = "Back";
            this.btn_Back.UseVisualStyleBackColor = false;
            this.btn_Back.Click += new System.EventHandler(this.btn_Back_Click);
            // 
            // Room_Bookings_Invites
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(456, 328);
            this.Controls.Add(this.btn_Back);
            this.Controls.Add(this.btn_Exit);
            this.Controls.Add(this.btn_Next);
            this.Controls.Add(this.lst_Empl);
            this.Controls.Add(this.lbl_Eleven);
            this.Controls.Add(this.lbl_Ten);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Name = "Room_Bookings_Invites";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Room Booking Invites";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Ten;
        private System.Windows.Forms.Label lbl_Eleven;
        private System.Windows.Forms.ListBox lst_Empl;
        private System.Windows.Forms.Button btn_Next;
        private System.Windows.Forms.Button btn_Exit;
        private System.Windows.Forms.Button btn_Back;
    }
}