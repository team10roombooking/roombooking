﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Room_Booking
{
    public partial class Room_Bookings_Invites : Form
    {

        Confirm_Booking confirm = new Confirm_Booking(); // creates a local variable for the Confirm_Booking form

        public Room_Bookings_Invites()
        {
            InitializeComponent();
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;            
            Close(); // Closes the Invite dialog box and returns to the booking form
        }

        private void btn_Next_Click(object sender, EventArgs e)
        {
            Visible = false;
            confirm.ShowDialog(); // opens the confirm booking form and hides the invite form once next button is clicked
            Visible = true;
        }

        private void btn_Back_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close(); //closes the confirm button and returns to the prieveous form that was used
        }
    }
}
