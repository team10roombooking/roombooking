﻿namespace Room_Booking
{
    partial class Room_Booking
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.lbl_One = new System.Windows.Forms.Label();
            this.lbl_Three = new System.Windows.Forms.Label();
            this.txt_One = new System.Windows.Forms.TextBox();
            this.txt_Two = new System.Windows.Forms.TextBox();
            this.lbl_Two = new System.Windows.Forms.Label();
            this.lbl_Four = new System.Windows.Forms.Label();
            this.lbl_Five = new System.Windows.Forms.Label();
            this.lbl_Six = new System.Windows.Forms.Label();
            this.drop_One = new System.Windows.Forms.ComboBox();
            this.drop_Two = new System.Windows.Forms.ComboBox();
            this.drop_Three = new System.Windows.Forms.ComboBox();
            this.lbl_Seven = new System.Windows.Forms.Label();
            this.lbl_Eight = new System.Windows.Forms.Label();
            this.lbl_Nine = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.rad_Yes = new System.Windows.Forms.RadioButton();
            this.rad_No = new System.Windows.Forms.RadioButton();
            this.lbl_Ten = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(342, 125);
            this.monthCalendar1.Margin = new System.Windows.Forms.Padding(14, 14, 14, 14);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 0;
            // 
            // lbl_One
            // 
            this.lbl_One.AutoSize = true;
            this.lbl_One.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_One.Location = new System.Drawing.Point(123, 14);
            this.lbl_One.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_One.Name = "lbl_One";
            this.lbl_One.Size = new System.Drawing.Size(441, 47);
            this.lbl_One.TabIndex = 1;
            this.lbl_One.Text = "Room Booking Service";
            // 
            // lbl_Three
            // 
            this.lbl_Three.AutoSize = true;
            this.lbl_Three.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Three.Location = new System.Drawing.Point(338, 88);
            this.lbl_Three.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Three.Name = "lbl_Three";
            this.lbl_Three.Size = new System.Drawing.Size(340, 29);
            this.lbl_Three.TabIndex = 2;
            this.lbl_Three.Text = "Please Select Date Required *";
            // 
            // txt_One
            // 
            this.txt_One.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_One.Location = new System.Drawing.Point(16, 125);
            this.txt_One.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_One.MaxLength = 50;
            this.txt_One.Name = "txt_One";
            this.txt_One.Size = new System.Drawing.Size(272, 35);
            this.txt_One.TabIndex = 3;
            // 
            // txt_Two
            // 
            this.txt_Two.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_Two.Location = new System.Drawing.Point(16, 205);
            this.txt_Two.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txt_Two.MaxLength = 8;
            this.txt_Two.Name = "txt_Two";
            this.txt_Two.Size = new System.Drawing.Size(272, 35);
            this.txt_Two.TabIndex = 4;
            this.txt_Two.TextChanged += new System.EventHandler(this.txt_Two_TextChanged);
            // 
            // lbl_Two
            // 
            this.lbl_Two.AutoSize = true;
            this.lbl_Two.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Two.Location = new System.Drawing.Point(10, 88);
            this.lbl_Two.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Two.Name = "lbl_Two";
            this.lbl_Two.Size = new System.Drawing.Size(209, 29);
            this.lbl_Two.TabIndex = 6;
            this.lbl_Two.Text = "Employee Name *";
            // 
            // lbl_Four
            // 
            this.lbl_Four.AutoSize = true;
            this.lbl_Four.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Four.Location = new System.Drawing.Point(10, 169);
            this.lbl_Four.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Four.Name = "lbl_Four";
            this.lbl_Four.Size = new System.Drawing.Size(231, 29);
            this.lbl_Four.TabIndex = 7;
            this.lbl_Four.Text = "Employee Number *";
            // 
            // lbl_Five
            // 
            this.lbl_Five.AutoSize = true;
            this.lbl_Five.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Five.Location = new System.Drawing.Point(10, 588);
            this.lbl_Five.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Five.Name = "lbl_Five";
            this.lbl_Five.Size = new System.Drawing.Size(202, 29);
            this.lbl_Five.TabIndex = 8;
            this.lbl_Five.Text = "* Required Fields";
            // 
            // lbl_Six
            // 
            this.lbl_Six.AutoSize = true;
            this.lbl_Six.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Six.Location = new System.Drawing.Point(10, 249);
            this.lbl_Six.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Six.Name = "lbl_Six";
            this.lbl_Six.Size = new System.Drawing.Size(200, 29);
            this.lbl_Six.TabIndex = 9;
            this.lbl_Six.Text = "Room Required *";
            // 
            // drop_One
            // 
            this.drop_One.AllowDrop = true;
            this.drop_One.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.drop_One.ForeColor = System.Drawing.SystemColors.GrayText;
            this.drop_One.FormattingEnabled = true;
            this.drop_One.Items.AddRange(new object[] {
            "Room One",
            "Room Two",
            "Room Three",
            "Room Four",
            "Room Five",
            "Conference Room One",
            "Conference Room Two"});
            this.drop_One.Location = new System.Drawing.Point(16, 285);
            this.drop_One.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.drop_One.Name = "drop_One";
            this.drop_One.Size = new System.Drawing.Size(272, 37);
            this.drop_One.TabIndex = 10;
            this.drop_One.Text = "Please Select One";
            // 
            // drop_Two
            // 
            this.drop_Two.ForeColor = System.Drawing.SystemColors.GrayText;
            this.drop_Two.FormattingEnabled = true;
            this.drop_Two.Items.AddRange(new object[] {
            "08:00",
            "09:00",
            "10:00",
            "11:00",
            "12:00",
            "13:00",
            "14:00",
            "15:00",
            "16:00",
            "17:00",
            "18:00",
            ""});
            this.drop_Two.Location = new System.Drawing.Point(16, 417);
            this.drop_Two.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.drop_Two.Name = "drop_Two";
            this.drop_Two.Size = new System.Drawing.Size(180, 28);
            this.drop_Two.TabIndex = 11;
            this.drop_Two.Text = "Select Time";
            // 
            // drop_Three
            // 
            this.drop_Three.ForeColor = System.Drawing.SystemColors.GrayText;
            this.drop_Three.FormattingEnabled = true;
            this.drop_Three.Items.AddRange(new object[] {
            "09:00",
            "10:00",
            "11:00",
            "12:00",
            "13:00",
            "14:00",
            "15:00",
            "16:00",
            "17:00",
            "18:00",
            "19:00"});
            this.drop_Three.Location = new System.Drawing.Point(224, 417);
            this.drop_Three.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.drop_Three.Name = "drop_Three";
            this.drop_Three.Size = new System.Drawing.Size(180, 28);
            this.drop_Three.TabIndex = 12;
            this.drop_Three.Text = "Select Time";
            // 
            // lbl_Seven
            // 
            this.lbl_Seven.AutoSize = true;
            this.lbl_Seven.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Seven.Location = new System.Drawing.Point(10, 343);
            this.lbl_Seven.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Seven.Name = "lbl_Seven";
            this.lbl_Seven.Size = new System.Drawing.Size(224, 29);
            this.lbl_Seven.TabIndex = 13;
            this.lbl_Seven.Text = "Please Select Time";
            // 
            // lbl_Eight
            // 
            this.lbl_Eight.AutoSize = true;
            this.lbl_Eight.Location = new System.Drawing.Point(14, 392);
            this.lbl_Eight.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Eight.Name = "lbl_Eight";
            this.lbl_Eight.Size = new System.Drawing.Size(56, 20);
            this.lbl_Eight.TabIndex = 14;
            this.lbl_Eight.Text = "From *";
            // 
            // lbl_Nine
            // 
            this.lbl_Nine.AutoSize = true;
            this.lbl_Nine.Location = new System.Drawing.Point(219, 392);
            this.lbl_Nine.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Nine.Name = "lbl_Nine";
            this.lbl_Nine.Size = new System.Drawing.Size(37, 20);
            this.lbl_Nine.TabIndex = 15;
            this.lbl_Nine.Text = "To *";
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.Color.SandyBrown;
            this.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.Location = new System.Drawing.Point(543, 574);
            this.btnExit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(129, 45);
            this.btnExit.TabIndex = 16;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btn_One_Click);
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNext.Location = new System.Drawing.Point(405, 574);
            this.btnNext.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(129, 45);
            this.btnNext.TabIndex = 17;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = false;
            this.btnNext.Click += new System.EventHandler(this.btn_Two_Click);
            // 
            // rad_Yes
            // 
            this.rad_Yes.AutoSize = true;
            this.rad_Yes.Checked = true;
            this.rad_Yes.Location = new System.Drawing.Point(16, 532);
            this.rad_Yes.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rad_Yes.Name = "rad_Yes";
            this.rad_Yes.Size = new System.Drawing.Size(62, 24);
            this.rad_Yes.TabIndex = 18;
            this.rad_Yes.TabStop = true;
            this.rad_Yes.Text = "Yes";
            this.rad_Yes.UseVisualStyleBackColor = true;
            // 
            // rad_No
            // 
            this.rad_No.AutoSize = true;
            this.rad_No.Location = new System.Drawing.Point(160, 532);
            this.rad_No.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.rad_No.Name = "rad_No";
            this.rad_No.Size = new System.Drawing.Size(54, 24);
            this.rad_No.TabIndex = 19;
            this.rad_No.Text = "No";
            this.rad_No.UseVisualStyleBackColor = true;
            // 
            // lbl_Ten
            // 
            this.lbl_Ten.AutoSize = true;
            this.lbl_Ten.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Ten.Location = new System.Drawing.Point(10, 483);
            this.lbl_Ten.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Ten.Name = "lbl_Ten";
            this.lbl_Ten.Size = new System.Drawing.Size(453, 29);
            this.lbl_Ten.TabIndex = 20;
            this.lbl_Ten.Text = "Would you like to inform other colleages?";
            // 
            // Room_Booking
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(702, 642);
            this.Controls.Add(this.lbl_Ten);
            this.Controls.Add(this.rad_No);
            this.Controls.Add(this.rad_Yes);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.lbl_Nine);
            this.Controls.Add(this.lbl_Eight);
            this.Controls.Add(this.lbl_Seven);
            this.Controls.Add(this.drop_Three);
            this.Controls.Add(this.drop_Two);
            this.Controls.Add(this.drop_One);
            this.Controls.Add(this.lbl_Six);
            this.Controls.Add(this.lbl_Five);
            this.Controls.Add(this.lbl_Four);
            this.Controls.Add(this.lbl_Two);
            this.Controls.Add(this.txt_Two);
            this.Controls.Add(this.txt_One);
            this.Controls.Add(this.lbl_Three);
            this.Controls.Add(this.lbl_One);
            this.Controls.Add(this.monthCalendar1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Room_Booking";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Room Booking";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label lbl_One;
        private System.Windows.Forms.Label lbl_Three;
        private System.Windows.Forms.TextBox txt_One;
        private System.Windows.Forms.TextBox txt_Two;
        private System.Windows.Forms.Label lbl_Two;
        private System.Windows.Forms.Label lbl_Four;
        private System.Windows.Forms.Label lbl_Five;
        private System.Windows.Forms.Label lbl_Six;
        private System.Windows.Forms.ComboBox drop_One;
        private System.Windows.Forms.ComboBox drop_Two;
        private System.Windows.Forms.ComboBox drop_Three;
        private System.Windows.Forms.Label lbl_Seven;
        private System.Windows.Forms.Label lbl_Eight;
        private System.Windows.Forms.Label lbl_Nine;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.RadioButton rad_Yes;
        private System.Windows.Forms.RadioButton rad_No;
        private System.Windows.Forms.Label lbl_Ten;
    }
}

